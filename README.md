# Instructions #

This README helps you to understand better how all the tasks run.

### TO RUN THIS PROJECT ###

This is a simple PHP structure based project. So you might just need to have PHP 7.* regularly installed in your 
environment, and run the following command in its root folder, through your terminal:

 `php -S 127.0.0.1:8000`
 
* Yes, you can use localhost
* Yes, you can change the port if you want to

That was just an example... Feel free to run it the way you`d enjoy :D

### Task 1: List countries and its capitals ###

* Lists five countries and its capitals;
* Default ascending ordered by capitals names;
* Order can be changed by clicking in "Countries capitals" header link

### Task 2: Has Joazinho bitten himself? ###

* Shows message if Joazinho has bitten himself;
* Class to control all actions;
* Two verification options: Random, or 50% true, 50% false;

### Task 3: Upload files and show its extensions ###

* Select multiple files to upload;
* Care about the POST Content-Length when uploading big files;
* Lists filename > its extension;
* List is ordered by files`s extensions;

### Task 4: User registration form ###

* Validates if user exists by email and login;
* Validates phonenumber format and email format;
* Stores users in local file `registros.txt`;

### Task 5: Convert XML file to CSV file ###

* Validates if xml file is a valid file;
* Validates file extension;
* Converts XML file to a CSV File;
* Donwloads it;
* `XML FILE IS IN ITS ROOT TASK DIRECTORY`

### Task 6: Create class to generate select HTML to a web form ###

* Creates class From to handle form request;
* Creates class to generate HTML for select component;
* Validates essentials;