<?php

$_SERVER['BASE_URL'] = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://' . $_SERVER['SERVER_NAME'];

if (!empty($_SERVER['SERVER_PORT'])) {

    $_SERVER['BASE_URL'] .= ":{$_SERVER['SERVER_PORT']}/";
}