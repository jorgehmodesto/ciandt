<?php
require_once 'bootstrap/web.php';
?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <link rel="stylesheet" href="resources/styles.css">

        <title>CI&T Test</title>
    </head>
    <body>
        <div class="row">
            <h1 class="main_title">CI&T Development skills test</h1>
            <div class="container">
                <div class="tasks-list">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="<?php echo "{$_SERVER['BASE_URL']}task1"; ?>">Task 1: List countries and its capitals</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo "{$_SERVER['BASE_URL']}task2"; ?>">Task 2: Has Joazinho bitten himself?</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo "{$_SERVER['BASE_URL']}task3"; ?>">Task 3: Upload files and show its extensions</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo "{$_SERVER['BASE_URL']}task4"; ?>">Task 4: User registration form</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo "{$_SERVER['BASE_URL']}task5"; ?>">Task 5: Convert XML file to CSV file</a>
                        </li>
                        <li class="list-group-item">
                            <a href="<?php echo "{$_SERVER['BASE_URL']}task6"; ?>">Task 6: Create class to generate select HTML to a web form</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>