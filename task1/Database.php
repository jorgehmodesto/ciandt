<?php

/**
 * Stores countries and principal cities records
 *
 * Class Database
 */
class Database
{
    /**
     * Property to store countries records
     *
     * @var array $countries
     */
    private $countries = [
        'usa' => 'United States of America',
        'br' => 'Brazil',
        'es' => 'Spain',
        'et' => 'Ethiopia',
        'ar' => 'Argentina'
    ];

    /**
     * Property to store principal cities records
     *
     * @var array $principalCities
     */
    private $principalCities = [
        'usa' => 'Washington',
        'br' => 'Brasilia',
        'es' => 'Madrid',
        'et' => 'Addis Ababa',
        'ar' => 'Buenos Aires'
    ];

    /**
     * Returns location records
     *
     * @var array $location
     */
    private $location = [];

    /**
     * Property to define order
     *
     * @var bool
     */
    public $descending = false;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        $this->handleRequest()
            ->setUpLocationContent();
    }

    /**
     * Returns countries list
     *
     * @return array
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Returns principal cities list
     *
     * @return array
     */
    public function getPrincipalCities()
    {
        $this->descending === true ? arsort($this->principalCities) : asort($this->principalCities);

        return $this->principalCities;
    }

    /**
     * Return location records
     *
     * @return array
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Returns country record by code
     *
     * @param string $countryCode
     * @return mixed|null
     */
    private function getCountryByCode(string $countryCode)
    {
        if (!isset($this->countries[$countryCode])) {

            return null;
        }

        return $this->countries[$countryCode];
    }

    /**
     * Sets location content up
     *
     * @return array
     */
    private function setUpLocationContent()
    {
        foreach ($this->getPrincipalCities() as $countryCode => $principalCityName)
        {
            $country = $this->getCountryByCode($countryCode);

            if (empty($country)) {

                continue;
            }

            $this->location[$country] = $principalCityName;
        }

        return $this->location;
    }

    /**
     * Method to handle request params
     *
     * @return $this
     */
    private function handleRequest()
    {
        if (isset($_GET['order']) && $_GET['order'] === 'desc') {

            $this->descending = true;
        }

        return $this;
    }
}