<?php
require_once '../bootstrap/web.php';
require_once 'Database.php';

$database = new Database();

$location = $database->getLocation();

$order = $database->descending === true ? "asc" : "desc";
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <link rel="stylesheet" href="../resources/styles.css">

        <title>Countries capitals</title>
    </head>
    <body>
        <div class="row">
            <div class="container">
                <h1 class="main_title">Countries capitals</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Countries capitals list</li>
                    </ol>
                </nav>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">
                                <?php echo "<a href='{$_SERVER['BASE_URL']}task1/?order={$order}'>Countries capitals</a>"; ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($location as $country => $capital) {

                        echo "<tr><td>{$country} capital is {$capital}</td></tr>";
                    }?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>