<?php

/**
 * Class Joaozinho
 */
class Joaozinho
{
    /**
     * Property to define action value
     *
     * @var integer $wasBitten
     */
    private $wasBitten;

    /**
     * Property to store bitten info message
     *
     * @var null $bittenMessage
     */
    private $bittenMessage = null;

    /**
     * Property to check check condition
     *
     * @var bool $random
     */
    private $random = false;

    /**
     * Joaozinho constructor.
     */
    public function __construct()
    {
        $this->handleRequest();
    }

    /**
     * @return integer
     */
    public function getWasBitten()
    {
        return $this->wasBitten;
    }

    /**
     * @param $wasBitten
     * @return $this
     */
    public function setWasBitten($wasBitten)
    {
        $this->wasBitten = $wasBitten;
        return $this;
    }

    /**
     * @return null
     */
    public function getBittenMessage()
    {
        return $this->bittenMessage;
    }

    /**
     * @param $bittenMessage
     * @return $this
     */
    public function setBittenMessage($bittenMessage)
    {
        $this->bittenMessage = $bittenMessage;
        return $this;
    }

    /**
     * @param $random
     * @return $this
     */
    public function setRandom($random)
    {
        $this->random = $random;
        return $this;
    }

    /**
     * @return bool
     */
    public function getRandom()
    {
        return $this->random;
    }

    /**
     * Method to handle request
     */
    private function handleRequest()
    {
        if (!empty($_POST)) {

            if (isset($_POST['wasBitten'])) {

                $this->setWasBitten($_POST['wasBitten']);
            }

            if (isset($_POST['checkRandom'])) {

                $this->setRandom($_POST['checkRandom'] == ASSERT_ACTIVE ? true : false);
            }

            $this->checkJoaozinhoWasBitten();
        }
    }

    /**
     * Method to verify if Joazinho was bitten
     */
    private function checkJoaozinhoWasBitten()
    {
        $negative = null;

        if ($this->getRandom() === true) {

            $wasBitten =  (bool)random_int(0, 1);
            $negative = $wasBitten === true ? '' : " <b>NOT</b>";

            $this->setBittenMessage("Joãozinho has{$negative} bitten himself");

            return;
        }

        if ($this->getWasBitten() != ASSERT_ACTIVE) {

            $negative = " <b>NOT</b>";
            $this->setWasBitten(ASSERT_ACTIVE);
        } else {
            $this->setWasBitten(0);
        }

        $this->setBittenMessage("Joãozinho has{$negative} bitten himself");
    }
}