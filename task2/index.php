<?php
require_once '../bootstrap/web.php';
require_once 'Joaozinho.php';

$joazinho = new Joaozinho();
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="../resources/styles.css">

    <title>CI&T Test - Has Joazinho bitten himself?</title>
</head>
<body>
<div class="row">
    <h1 class="main_title">Check if Joazinho has bitten himself</h1>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Check if Joazinho has bitten himself</li>
            </ol>
        </nav>
        <form method="post" action="<?php echo "{$_SERVER['BASE_URL']}task2"; ?>" name="frmJoaozinho" id="frmJoazinho">
            <input type="hidden" name="wasBitten" id="wasBitten" value="<?php echo $joazinho->getWasBitten(); ?>"/>
            <div class="form-group">
                <div class="col-md-12">
                    <label for="">Select the way you want to check it</label>
                    <select class="form-control" name="checkRandom" id="checkRandom">
                        <option value="1">Random</option>
                        <option value="0" <?php echo $joazinho->getRandom() === false ? "selected" : ''; ?>>50% true, 50% false</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12" style="text-align: right">
                <button type="submit" class="btn btn-primary">Check it out!</button>
            </div>
            <?php
            if (!empty($joazinho->getBittenMessage())) { ?>
                <div class="mg-top-10 alert alert-secondary" role="alert">
                    <?php echo $joazinho->getBittenMessage(); ?>
                </div>
                <?php
            }?>
        </form>
    </div>
</div>
</body>
</html>