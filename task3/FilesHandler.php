<?php

/**
 * Class FilesHandler
 */
class FilesHandler
{
    /**
     * Property to store uploaded file names
     *
     * @var array $files
     */
    private $files = [];

    /**
     * Property to store processed files
     *
     * @var array
     */
    private $processedFiles = [];

    /**
     * Property to store occurred errors
     *
     * @var array
     */
    private $errors = [];

    /**
     * FilesHandler constructor.
     */
    public function __construct()
    {
        $this->handleRequest();
    }

    /**
     * @param array $files
     * @return $this
     */
    public function setFiles(array $files)
    {
        $this->files = $files;
        return $this;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $processedFiles
     * @return $this
     */
    public function setProcessedFiles(array $processedFiles)
    {
        $this->processedFiles = $processedFiles;
        return $this;
    }

    /**
     * @return array
     */
    public function getProcessedFiles()
    {
        return $this->processedFiles;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return $this
     */
    private function handleRequest()
    {
        if (!empty($_FILES)) {

            if (!empty($_FILES['files']['name'][0])) {

                $this->setFiles($_FILES['files']['name']);
            }

            $this->processFiles();
        }

        return $this;
    }

    /**
     * Method to process uploaded files
     *
     * @return $this
     */
    private function processFiles()
    {
        if (empty($this->getFiles())) {

            $this->pushErrors("You should select at least one file to be processed");

            return $this;
        }

        $processedFiles = [];

        foreach ($this->getFiles() as $fileName) {

            $extension = $this->getFileExtension($fileName);

            if (empty($extension)) {

                $this->pushErrors("The file <b>{$fileName}</b> does not have a valid extension");
                continue;
            }

            $processedFiles[$extension] = $fileName;
        }

        ksort($processedFiles);

        $this->setProcessedFiles($processedFiles);

        return $this;
    }

    /**
     * Method to extract file extension
     *
     * @param string $filename
     * @return mixed
     */
    private function getFileExtension(string $filename)
    {
        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    /**
     * Method to push errors
     *
     * @param string $error
     * @return $this
     */
    private function pushErrors(string $error)
    {
        array_push($this->errors, $error);
        return $this;
    }

}