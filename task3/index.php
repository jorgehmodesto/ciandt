<?php
require_once '../bootstrap/web.php';
require_once 'FilesHandler.php';

$filesHandler = new FilesHandler();
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="../resources/styles.css">

    <title>CI&T Test - Show and order files extensions</title>
</head>
<body>
<div class="row">
    <h1 class="main_title">Show and order files extensions</h1>
    <div class="container">
        <?php
        if ($filesHandler->getErrors()) { ?>
            <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Ooops! The following error(s) occurred!</h4>
                <ul>
                    <?php
                    foreach ($filesHandler->getErrors() as $error) {
                        echo "<li>{$error}</li>";
                    }
                    ?>
                </ul>
            </div>
        <?php
        }?>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Show and order files extensions</li>
            </ol>
        </nav>
        <div class="col-lg-12">
            <form action="<?php echo "{$_SERVER['BASE_URL']}task3"?>" method="post" name="frmFiles" id="frmFiles" enctype="multipart/form-data">
                <input id="files" name="files[]" type="file" multiple />
                <div class="col-md-12" style="text-align: right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            <?php
            if (count($filesHandler->getProcessedFiles())) { ?>
                <table class="table mg-top-10">
                    <thead class="thead-dark">
                    <tr>
                        <th>
                            Filename
                        </th>
                        <th scope="col">
                            File extension
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($filesHandler->getProcessedFiles() as $extension => $filename) {

                        echo "<tr><td>{$filename}</td><td>.{$extension}</td></tr>";
                    } ?>
                    </tbody>
                </table>
            <?php
            }?>
        </div>
    </div>
</div>
</body>
</html>
