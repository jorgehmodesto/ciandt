<?php

/**
 * Class DB
 */
class DB
{
    /**
     * Stored records file
     *
     * @var string $file
     */
    private $file;

    /**
     * Stores records reconnection
     *
     * @var array
     */
    private $connection = [];

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->setFile('storage/registros.txt');
    }

    /**
     * @param string $file
     * @return $this
     */
    public function setFile(string $file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function getConnection()
    {
        if (file_exists($this->getFile())) {

            $contents = file_get_contents($this->getFile());

            $this->connection = [];

            if (!empty($contents)) {

                $this->connection = json_decode($contents, true);
            }
        }

        return $this;
    }

    /**
     * Method to find record by criteria
     *
     * @param string $field
     * @param string $value
     * @return null
     */
    public function findBy(string $field, string $value)
    {
        if (!empty($this->connection)) {

            foreach ($this->connection as $id => $record) {

                if (array_key_exists($field, $record)) {

                    if ($record[$field] === $value) {

                        return $record;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Method to save record to database
     *
     * @param $record
     * @return bool
     */
    public function save(array $record)
    {
        if (!empty($record)) {

            array_push($this->connection, $record);

            $records = json_encode($this->connection);

            if (!empty($records)) {

                file_put_contents($this->getFile(), $records);

                return true;
            }
        }

        return false;
    }
}