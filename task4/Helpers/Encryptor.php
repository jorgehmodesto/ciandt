<?php

/**
 * Class Encryptor
 */
class Encryptor
{
    /**
     * Method to encrypt passwords
     *
     * @param string $password
     * @return string
     */
    public static function password(string $password)
    {
        return sha1(md5($password));
    }
}