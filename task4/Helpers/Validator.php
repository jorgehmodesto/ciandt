<?php

/**
 * Class Validator
 */
class Validator
{
    /**
     * Method to validate if provided email is a valid email format
     *
     * @param string $email
     * @return bool
     */
    public static function email(string $email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Method to validate if phone number is in valid format
     * @defaultFormat (99) 99999-9999
     * @param string $phone
     * @return bool
     */
    public static function phone(string $phone)
    {
        if (preg_match("/^\(?\d{2}\)?\s?\d{5}\-?\d{4}$/", $phone)) {

            return true;
        }

        return false;
    }
}