<?php

require_once '../Exceptions/ValidationException.php';
require_once 'Helpers/Validator.php';
require_once 'DB.php';
require_once 'Helpers/Encryptor.php';

/**
 * Class Record
 */
class Record extends DB
{
    /**
     * @var array $postFields
     */
    private $postFields = [];

    /**
     * @var array $errors
     */
    private $errors = [];

    /**
     * Property to identify if the request was successfully executed
     *
     * @var bool $success
     */
    private $success = false;

    /**
     * Record constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->handleRequest();
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $error
     * @return $this
     */
    public function pushError(string $error)
    {
        array_push($this->errors, $error);
        return $this;
    }

    /**
     * @return bool
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @param array $post
     * @return $this
     */
    public function setPostFields(array $post)
    {
        $this->postFields = $post;
        return $this;
    }

    /**
     * @param string|null $field
     * @return array|mixed|null
     */
    public function getPostFields(string $field = null)
    {
        if (!empty($field)) {

            if (isset($this->postFields[$field])) {

                return $this->postFields[$field];
            }

            return null;
        }

        return $this->postFields;
    }

    /**
     * @return $this
     */
    private function handleRequest()
    {
        try {

            if (!empty($_POST)) {

                $this->setPostFields($_POST);

                $this->validateRequest();
                $this->validateIfUserExists();

                $record = $this->getPostFields();
                $record['password'] = Encryptor::password($record['password']);

                $this->getConnection()
                    ->save($record);

                $this->setSuccess(true);
            }

            return $this;

        } catch (ValidationException $e) {

            $this->pushError($e->getMessage());
            return $this;
        }
    }

    /**
     * Method to validate request
     *
     * @throws ValidationException
     */
    private function validateRequest()
    {
        if (empty($_POST['name'])) {

            throw new ValidationException('Field name is mandatory, and was not provided');
        }

        if (empty($_POST['surname'])) {

            throw new ValidationException('Field surname is mandatory, and was not provided');
        }

        if (empty($_POST['email'])) {

            throw new ValidationException('Field email is mandatory, and was not provided');
        }

        if (empty($_POST['phone'])) {

            throw new ValidationException('Field phone is mandatory, and was not provided');
        }

        if (empty($_POST['login'])) {

            throw new ValidationException('Field login is mandatory, and was not provided');
        }

        if (empty($_POST['password'])) {

            throw new ValidationException('Field password is mandatory, and was not provided');
        }

        if (!Validator::email($this->getPostFields('email'))) {

            throw new ValidationException('The provided email format is not valid');
        }

        if (!Validator::phone($this->getPostFields('phone'))) {

            throw new ValidationException('The provided phone format is not valid. Pattern: <b>(99) 99999-9999</b>');
        }
    }

    /**
     * Method to validate user existence
     *
     * @throws ValidationException
     */
    private function validateIfUserExists()
    {
        $emailExists = $this->getConnection()
            ->findBy('email', $this->getPostFields('email'));

        if (!empty($emailExists)) {

            throw new ValidationException('The provided email is already registered');
        }

        $loginExists = $this->getConnection()
            ->findBy('login', $this->getPostFields('login'));

        if (!empty($loginExists)) {

            throw new ValidationException('The provided login is already registered');
        }
    }
}