<?php
require_once '../bootstrap/web.php';
require_once  'Record.php';

$record = new Record();
?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <link rel="stylesheet" href="../resources/styles.css">

        <title>CI&T Test - Save form records</title>
    </head>
    <body>
        <div class="row">
            <h1 class="main_title">User registration form</h1>
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Validate user form records and store</li>
                    </ol>
                </nav>
                <?php
                if ($record->getErrors()) { ?>
                    <div class="alert alert-danger" role="alert">
                        <h4 class="alert-heading">Ooops! The following error(s) occurred!</h4>
                        <ul>
                            <?php
                            foreach ($record->getErrors() as $error) {
                                echo "<li>{$error}</li>";
                            }
                            ?>
                        </ul>
                    </div>
                <?php
                }?>
                <?php
                if ($record->getSuccess() === true) { ?>
                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Nice \o/</h4>
                        <p>The record was successfully stored!</p>
                    </div>
                <?php
                }?>
                <form method="post" action="<?php echo $_SERVER['BASE_URL']?>task4" name="frmUser" id="frmUser">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name here" value="<?php echo $record->getPostFields('name'); ?>" />
                            </div>
                            <div class="col-md-4">
                                <label for="surname">Surname</label>
                                <input type="text" class="form-control" id="surname" name="surname" placeholder="Enter your surname here" value="<?php echo $record->getPostFields('surname'); ?>" />
                            </div>
                            <div class="col-md-4">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email here" value="<?php echo $record->getPostFields('email'); ?>" />
                            </div>
                        </div>
                        <div class="row mg-top-10">
                            <div class="col-md-4">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter your phone here" value="<?php echo $record->getPostFields('phone'); ?>" />
                            </div>
                            <div class="col-md-4">
                                <label for="login">Login</label>
                                <input type="text" class="form-control" id="login" name="login" placeholder="Enter your login here" value="<?php echo $record->getPostFields('login'); ?>" />
                            </div>
                            <div class="col-md-4">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter your password here" value="<?php echo $record->getPostFields('password'); ?>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mg-top-10" style="text-align: right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>