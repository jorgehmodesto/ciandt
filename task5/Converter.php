<?php
require_once '../Exceptions/ValidationException.php';

/**
 * Class Converter
 */
class Converter
{
    /**
     * @var array $errors
     */
    private $errors = [];

    /**
     * Property to show if the request has been successfully executed
     *
     * @var bool $success
     */
    private $success = false;

    /**
     * @var array $file
     */
    private $file;

    /**
     * @var SimpleXMLElement $xmlElement
     */
    private $xmlElement;

    /**
     * Converter constructor.
     */
    public function __construct()
    {
        $this->handleRequest();
    }

    /**
     * @return bool
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @param array $file
     * @return $this
     */
    public function setFile(array $file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return array
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $error
     * @return $this
     */
    private function pushError(string $error)
    {
        array_push($this->errors, $error);
        return $this;
    }

    /**
     * @return $this
     */
    private function handleRequest()
    {
        try {

            if (!empty($_FILES)) {

                if (empty($_FILES['file']['name'])) {

                    throw new ValidationException('You should upload a XML file to convert');
                }

                $this->setFile($_FILES['file']);
                $this->validateFile();
                $this->mount();
                $this->convertToCsv($this->xmlElement);
                $this->download();
            }

            return $this;

        } catch (ValidationException $vE) {

            $this->pushError($vE->getMessage());
            return $this;
        }
    }

    /**
     * Method to validate file
     *
     * @throws ValidationException
     */
    private function validateFile()
    {
        $file = $this->getFile();

        if ($file['type'] !== 'text/xml') {

            throw new ValidationException('The provided file should be a <b>XML</b> file');
        }
    }

    /**
     * @throws ValidationException
     */
    private function mount()
    {
        libxml_use_internal_errors(true);
        $xmlElement = simplexml_load_file($this->file['tmp_name']);

        if (!$xmlElement) {

            throw new ValidationException('XML content is not valid');
        }

        $this->xmlElement = $xmlElement;
    }

    /**
     * @param SimpleXMLElement $xml
     */
    private function convertToCsv(SimpleXMLElement $xml)
    {
        $csvFileName = pathinfo($this->file['name'], PATHINFO_FILENAME) . '.csv';
        $csv = fopen("tmp/{$csvFileName}", 'w');

        $header = ['to', 'from', 'heading', 'body'];

        fputcsv($csv, $header, ';');

        /**
         * @var SimpleXMLElement $content
         */
        foreach ($xml->content as $content) {

            $line = [
                (string)$content->to,
                (string)$content->from,
                (string)$content->heading,
                (string)$content->body,
            ];

            fputcsv($csv, $line, ';');
        }

        fclose($csv);
    }

    /**
     * @return $this
     */
    private function download()
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename('tmp/note.csv').'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize('tmp/note.csv'));
        flush();
        readfile('tmp/note.csv');

        return $this;
    }
}