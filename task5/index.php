<?php
require_once '../bootstrap/web.php';
require_once 'Converter.php';

$converter = new Converter();
?>

<!doctype html>
    <html lang="en">
    <head>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        <link rel="stylesheet" href="../resources/styles.css">

        <title>CI&T Test - XML to CSV converter</title>
    </head>
    <body>
        <div class="row">
            <h1 class="main_title">XML to CSV converter</h1>
            <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">XML to CSV converter</li>
                    </ol>
                    <?php
                    if ($converter->getErrors()) { ?>
                        <div class="alert alert-danger" role="alert">
                            <h4 class="alert-heading">Ooops! The following error(s) occurred!</h4>
                            <ul>
                                <?php
                                foreach ($converter->getErrors() as $error) {
                                    echo "<li>{$error}</li>";
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }?>
                    <?php
                    if ($converter->getSuccess() === true) { ?>
                        <div class="alert alert-success" role="alert">
                            <h4 class="alert-heading">Nice \o/</h4>
                            <p>The record was successfully stored!</p>
                        </div>
                        <?php
                    }?>
                    <form action="<?php echo "{$_SERVER['BASE_URL']}task5"?>" method="post" name="frmConverter" id="frmConverter" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="file" class="form-control" name="file">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 mg-top-10" style="text-align: right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </nav>
            </div>
        </div>
    </body>
</html>