<?php

/**
 * Class Form
 */
class Form
{
    /**
     * @var array $postFields
     */
    private $postFields;

    /**
     * @var array $errors
     */
    private $errors = [];

    public function __construct()
    {
        $this->handleRequest();
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return "{$_SERVER['BASE_URL']}task6";
    }

    /**
     * Method to return one post field value or the entire post request
     *
     * @param string|null $key
     * @return array|mixed|null
     */
    public function getPostFields(string $key = null)
    {
        if (empty($key)) {

            return $this->postFields;
        }

        if (isset($this->postFields[$key])) {

            return $this->postFields[$key];
        }

        return null;
    }

    /**
     * @param array $errors
     * @return $this
     */
    public function mergeErrors(array $errors)
    {
        foreach ($errors as $error) {
            array_push($this->errors, $error);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $postFields
     * @return $this
     */
    private function setPostFields(array $postFields)
    {
        $this->postFields = $postFields;
        return $this;
    }

    /**
     * @return $this
     */
    private function handleRequest()
    {
        if (!empty($_POST)) {

            $this->setPostFields($_POST);
            return $this;
        }
    }
}