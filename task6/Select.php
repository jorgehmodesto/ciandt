<?php
require_once '../Exceptions/ValidationException.php';

/**
 * Class Select
 */
class Select
{
    /**
     * @var array $attr
     */
    private $attr = [];

    /**
     * @var string $label
     */
    private $label;

    /**
     * @var string $value
     */
    private $value;

    /**
     * @var array $options
     */
    private $options;

    /**
     * @var array $errors
     */
    private $errors = [];

    /**
     * @var string $id
     */
    private $id;

    /**
     * @var null
     */
    private $html = null;

    /**
     * Select constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        if (!empty($config['label'])) {

            $this->setLabel($config['label']);
        }

        if (!empty($config['value'])) {

            $this->setValue($config['value']);
        }

        if (!empty($config['options'])) {

            $this->setOptions($config['options']);
        }

        if (!empty($config['attributes'])) {

            foreach ($config['attributes'] as $attribute) {

                $this->pushAttribute($attribute);
            }
        }

        $this->process();
    }

    /**
     * @param array $attr
     * @return $this
     */
    public function pushAttribute(array $attr)
    {
        if (!empty($attr)) {

            array_push($this->attr, $attr);
        }

        return $this;
    }

    /**
     * @param string $label
     * @return $this
     */
    private function setLabel(string $label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attr;
    }

    /**
     * @param string $value
     * @return $this
     */
    private function setValue(string $value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param array $options
     */
    private function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $error
     * @return $this
     */
    private function pushError(string $error)
    {
        array_push($this->errors, $error);
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Method to mount HTML content
     *
     * @return null|string
     */
    private function process()
    {
        try {

            $attributes = null;

            if (empty($this->getLabel())) {

                throw new ValidationException('Select object label is missing. Please, check it out');
            }

            foreach ($this->getAttributes() as $attribute) {

                $attributeKeysErrorMessage = "%s key not found for your %s select attribute. Please, check it out";

                if (!isset($attribute['name'])) {

                    throw new ValidationException(sprintf($attributeKeysErrorMessage, 'Name', $this->getLabel()));
                }

                if (!isset($attribute['value'])) {

                    throw new ValidationException(sprintf($attributeKeysErrorMessage, 'Value', $this->getLabel()));
                }

                if ($attribute['name'] === 'id') {

                    $this->id = $attribute['value'];
                }

                $attributes .= " {$attribute['name']}='{$attribute['value']}'";
            }

            $optionsDefaultErrorMessage = "Some option {$this->getLabel()}`s %s was not defined. Please, check it out";

            $options = null;

            foreach ($this->getOptions() as $option) {

                $selected = null;

                if (empty($option['label'])) {

                    throw new ValidationException(sprintf($optionsDefaultErrorMessage, 'label'));
                }

                if (empty($option['value'])) {

                    throw new ValidationException(sprintf($optionsDefaultErrorMessage, 'value'));
                }

                if ($option['value'] == $this->getValue()) {

                    $selected="selected";
                }

                $options .= "<option value='{$option['value']}' {$selected}>{$option['label']}</option>";
            }

            $this->html = <<<HTML
                <select {$attributes} class="form-control">
                    {$options}
                </select>
HTML;

            return $this;

        } catch(ValidationException $vE) {
            $this->pushError($vE->getMessage());
            return null;
        }
    }

    /**
     * @return string
     */
    public function asHtml()
    {
        return $this->html;
    }
}