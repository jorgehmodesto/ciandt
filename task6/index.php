<?php
require_once '../bootstrap/web.php';
require_once 'Form.php';
require_once 'Select.php';

$form = new Form();

$selectGender = new Select([
    'value' => $form->getPostFields('gender'),
    'options' => [
        ['value' => 'female', 'label' => 'Female'],
        ['value' => 'male', 'label' => 'Male'],
        ['value' => 'undefined', 'label' => 'Undefined'],
    ],
    'label' => 'Gender',
    'attributes' => [
        ['name' => 'id', 'value' => 'slcGender'],
        ['name' => 'name', 'value' => 'gender'],
    ],
]);

if (!empty($selectGender->getErrors())) {

    $form->mergeErrors($selectGender->getErrors());
}

$selectActive = new Select([
    'value' => $form->getPostFields('active'),
    'options' => [
        ['value' => 'y', 'label' => 'Yes'],
        ['value' => 'n', 'label' => 'No'],
    ],
    'label' => 'Active',
    'attributes' => [
        ['name' => 'id', 'value' => 'slcActive'],
        ['name' => 'name', 'value' => 'active'],
    ],
]);

if (!empty($selectActive->getErrors())) {

    $form->mergeErrors($selectActive->getErrors());
}

$selectRole = new Select([
    'value' => $form->getPostFields('role'),
    'options' => [
        ['value' => 'admin', 'label' => 'Administrator'],
        ['value' => 'user', 'label' => 'User'],
        ['value' => 'guest', 'label' => 'Guest'],
    ],
    'label' => 'Role',
    'attributes' => [
        ['name' => 'id', 'value' => 'slcRole'],
        ['name' => 'name', 'value' => 'role'],
    ],
]);

if (!empty($selectRole->getErrors())) {

    $form->mergeErrors($selectRole->getErrors());
}

?>

<!doctype html>
<html lang="en">
<head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="../resources/styles.css">

    <title>CI&T Test - Save form records</title>
</head>
<body>
<div class="row">
    <h1 class="main_title">User registration form</h1>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Creates class to generate select field HTML</li>
            </ol>
        </nav>
        <?php
        if ($form->getErrors()) { ?>
            <div class="alert alert-danger" role="alert">
                <h4 class="alert-heading">Ooops! The following error(s) occurred!</h4>
                <ul>
                    <?php
                    foreach ($form->getErrors() as $error) {
                        echo "<li>{$error}</li>";
                    }
                    ?>
                </ul>
            </div>
            <?php
        }?>
        <form method="post" action="<?php echo $form->getAction()?>" name="frmUser" id="frmUser">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name here" value="<?php echo $form->getPostFields('name'); ?>" />
                    </div>
                    <div class="col-md-4">
                        <label for="surname">Surname</label>
                        <input type="text" class="form-control" id="surname" name="surname" placeholder="Enter your surname here" value="<?php echo $form->getPostFields('surname'); ?>" />
                    </div>
                    <div class="col-md-4">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email here" value="<?php echo $form->getPostFields('email'); ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-4">
                        <label for="<?php echo $selectGender->getId()?>"><?php echo $selectGender->getLabel()?></label>
                        <?php print $selectGender->asHtml()?>
                    </div>
                    <div class="col-md-4">
                        <label for="<?php echo $selectActive->getId()?>"><?php echo $selectActive->getLabel()?></label>
                        <?php print $selectActive->asHtml()?>
                    </div>
                    <div class="col-md-4">
                        <label for="<?php echo $selectRole->getId()?>"><?php echo $selectRole->getLabel()?></label>
                        <?php print $selectRole->asHtml()?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12" style="text-align: right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>